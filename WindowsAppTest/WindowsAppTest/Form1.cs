﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsAppTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtWelcome.Text == "")
            {
                txtWelcome.Text = "Welcome to CIS-2225";
            }
            else
            {
                txtWelcome.Text = "Welcome " + txtWelcome.Text + " to CIS-2225";
            }
            

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtWelcome.Text = "";
            lblMessage.Text = "";
        }

        private void btnMessage_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "Hello Everyone.";
        }
    }
}
